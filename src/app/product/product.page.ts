import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../models/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  product: Product = {
      presupuesto: 0,
      unidad: '',
      producto: '',
      cantidad: 0,
      valor_unitario: 0,
      valor_total: 0,
      fecha_adquisicion: '',
      proveedor: ''
  }; // Define una propiedad product de tipo Product
isEditing: any;

  constructor(private route: ActivatedRoute, private productService: ProductService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id !== null) {
        // Cargar detalles del producto utilizando el servicio ProductService
        // Ejemplo: this.product = this.productService.getProductById(id);
      } else {
        console.error('El ID del producto es nulo.');
      }
    });
  }

  saveProduct() {
    if (this.isEditing) {
      // Actualizar producto existente
      this.productService.updateProduct(this.product.id!, this.product)
        .then(() => {
          console.log('Producto actualizado correctamente.');
          // Aquí podrías redirigir al usuario a otra página o realizar otras acciones necesarias después de guardar.
        })
        .catch(error => {
          console.error('Error al actualizar el producto:', error);
          // Aquí podrías manejar el error de alguna manera, por ejemplo, mostrando un mensaje al usuario.
        });
    } else {
      // Crear nuevo producto
      this.productService.createProduct(this.product)
        .then(() => {
          console.log('Producto creado correctamente.');
          // Aquí podrías redirigir al usuario a otra página o realizar otras acciones necesarias después de guardar.
        })
        .catch(error => {
          console.error('Error al crear el producto:', error);
          // Aquí podrías manejar el error de alguna manera, por ejemplo, mostrando un mensaje al usuario.
        });
    }
  }
  

  // Puedes agregar más métodos y lógica según sea necesario
}
