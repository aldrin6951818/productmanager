import { Injectable } from '@angular/core';
import axios from 'axios';
import { Product } from '../models/product';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  baseUrl: string = 'http://localhost:3306/product_manager';

  async getAllProducts(): Promise<Product[]> {
    const response = await axios.get(this.baseUrl);
    return response.data;
  }

  async createProduct(product: Product) {
    const response = await axios.post(this.baseUrl, product);
    return response.data;
  }

  async updateProduct(id: number, product: Product) {
    const response = await axios.put(`${this.baseUrl}/${id}`, product);
    return response.data;
  }

  async deleteProduct(id: number) {
    const response = await axios.delete(`${this.baseUrl}/${id}`);
    return response.data;
  }
}
