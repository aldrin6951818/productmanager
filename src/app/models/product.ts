export interface Product {
    id?: number;
    presupuesto: number;
    unidad: string;
    producto: string;
    cantidad: number;
    valor_unitario: number;
    valor_total: number;
    fecha_adquisicion: string;
    proveedor: string;
  }
  