import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { NavController } from '@ionic/angular';
import { Product } from '../models/product';

@Component({
    selector: 'app-home', // Asegúrate de que el selector sea 'app-home'
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
  })
  
export class HomePage implements OnInit {
  products: Product[] = [];
  filteredProducts: Product[] = [];
  searchTerm: string = '';

  constructor(private productService: ProductService, private navCtrl: NavController) {}

  ngOnInit() {
    this.loadProducts();
  }

  async loadProducts() {
    this.products = await this.productService.getAllProducts();
    this.filteredProducts = this.products;
  }

  filterProducts() {
    this.filteredProducts = this.products.filter(product => {
      return product.producto.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
             product.unidad.toLowerCase().includes(this.searchTerm.toLowerCase());
    });
  }

  addProduct() {
    this.navCtrl.navigateForward('/product');
  }

  editProduct(product: Product) {
    // Verifica si 'id' tiene un valor antes de utilizarlo
    if (product.id !== undefined) {
      // Utiliza 'id' solo si tiene un valor definido
      this.navCtrl.navigateForward(`/product/${product.id}`);
    } else {
      // Si 'id' es undefined, puedes manejar este caso según tu lógica de negocio
      console.error('El ID del producto es indefinido.');
    }
  }
  async deleteProduct(id: number | undefined) {
    if (id !== undefined) {
      await this.productService.deleteProduct(id);
      this.loadProducts();
    } else {
      console.error('El ID del producto es indefinido.');
    }
  }
}  
