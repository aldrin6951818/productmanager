import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HomePage } from './home.page';
import { HomeRoutingModule } from './home-routing.module'; // Importa HomeRoutingModule

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    HomeRoutingModule // Usa HomeRoutingModule aquí
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
