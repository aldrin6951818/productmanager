const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();
const app = express();
const port = 3306;

app.use(bodyParser.json());
app.use(cors());

const db = new sqlite3.Database(':memory:');

db.serialize(() => {
    db.run(`CREATE TABLE products (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        presupuesto REAL,
        unidad TEXT,
        producto TEXT,
        cantidad INTEGER,
        valor_unitario REAL,
        valor_total REAL,
        fecha_adquisicion TEXT,
        proveedor TEXT
    )`);
});

app.post('/products', (req, res) => {
    const { presupuesto, unidad, producto, cantidad, valor_unitario, valor_total, fecha_adquisicion, proveedor } = req.body;
    db.run(`INSERT INTO products (presupuesto, unidad, producto, cantidad, valor_unitario, valor_total, fecha_adquisicion, proveedor) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`,
        [presupuesto, unidad, producto, cantidad, valor_unitario, valor_total, fecha_adquisicion, proveedor],
        function(err) {
            if (err) {
                return res.status(500).send(err.message);
            }
            res.status(201).send({ id: this.lastID });
        });
});

app.get('/products', (req, res) => {
    db.all('SELECT * FROM products', [], (err, rows) => {
        if (err) {
            return res.status(500).send(err.message);
        }
        res.status(200).json(rows);
    });
});

app.put('/products/:id', (req, res) => {
    const { id } = req.params;
    const { presupuesto, unidad, producto, cantidad, valor_unitario, valor_total, fecha_adquisicion, proveedor } = req.body;
    db.run(`UPDATE products SET presupuesto = ?, unidad = ?, producto = ?, cantidad = ?, valor_unitario = ?, valor_total = ?, fecha_adquisicion = ?, proveedor = ? WHERE id = ?`,
        [presupuesto, unidad, producto, cantidad, valor_unitario, valor_total, fecha_adquisicion, proveedor, id],
        function(err) {
            if (err) {
                return res.status(500).send(err.message);
            }
            res.status(200).send({ changes: this.changes });
        });
});

app.delete('/products/:id', (req, res) => {
    const { id } = req.params;
    db.run('DELETE FROM products WHERE id = ?', id, function(err) {
        if (err) {
            return res.status(500).send(err.message);
        }
        res.status(200).send({ changes: this.changes });
    });
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
